import { ChangePassword, Logout } from "./api_connector.js";
import settings from './settings.js';

$('#loadData').hide();

// Use the submit
document.querySelector('#changePasswordForm').addEventListener('submit', async (event) => {
  event.preventDefault();

  // Show loading spinner
  const loadingSpinner = document.getElementById("loading-spinner");
  loadingSpinner.style.display = "block";

  let currentPassword = document.querySelector('#inputCurrentPassword').value;
  let newPassword = document.querySelector('#inputNewPassword').value;
  let newPasswordConfirm = document.querySelector('#inputNewPasswordConfirm').value;
  const errorMessage = document.getElementById("errorMessage");

  // Clear previous & show messages
  // Check current pwd is ok
  if (currentPassword === "") {
    errorMessage.textContent += "Please enter your current password. ";
  }
  if (errorMessage.textContent == "" && currentPassword.length < settings.password_min_length) {
    errorMessage.textContent += "Invalid current password.";
  }
  // Check new pwd fits
  if (newPassword.length < settings.password_min_length) {
    errorMessage.textContent += "Please enter a new password of at least " + settings.password_min_length + " characters. ";
  }
  if (newPassword != newPasswordConfirm) {
    errorMessage.textContent += "Please repeat your password. ";
  }
  if (errorMessage.textContent != "") {
    return;
  };

  // Handle the API response
  let changePasswordTry = await ChangePassword(currentPassword, newPassword);
  if (changePasswordTry[0]) {
    currentPassword = "";
    newPassword = "";
    newPasswordConfirm = "";
    alert(" OK ! New password successfully assigned ");
    Logout();
    window.location.replace("login.html");
  } else {
    const errorReading = settings.errorcodes[changePasswordTry[1]];
    errorMessage.innerHTML = errorReading ? (errorReading + " ( Message from server : " + changePasswordTry[2] + " )") : "Server connection failed.";
  }


  // Hide loading spinner
  loadingSpinner.style.display = "none";
});