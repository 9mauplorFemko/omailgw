import { GetServerList, GetTransport, AddTransport, DeleteTransport } from "./api_connector.js";
//import settings from './settings.js';

// loading spinner + errorMessage
const alert = $("#alert");
alert.hide();

jQuery(function () {
  alert.show();
  alert.removeClass().addClass("alert alert-info");
  alert.html("Loading data...");
  UpdateTransportList();
  $("#todaydate").html(moment().format("YYYY-MM-DD"));
  $('#mapway').hide();
});

async function UpdateTransportList() {
  // Ask infos from API : Reports Settings
  let getServer = await GetServerList();
  if (getServer[0]) {
    let dataGetServers = getServer[1];
    var server = {};
    // Reset
    $('#hostname').prop('selectedIndex', 0).html('');
    let colors = [];
    let colorsAmount = 0;
    let defaultcolors = ["blue", "green", "red", "orange", "purple"];
    let domains = [];
    $('#mapping').html('');
    for (let dataGetServer of dataGetServers) {
      // create select options
      server[dataGetServer['hostname']] = dataGetServer['last_cli_update'];
      $('#hostname').append($('<option>', {
        value: dataGetServer['hostname'],
        text: dataGetServer['hostname']
      }));
      // format transport hostname
      if (!dataGetServer.domain.endsWith(".")) {
        dataGetServer.domain += ".";
      }
      let address = dataGetServer.hostname + "." + dataGetServer.domain;
      // create server's card if not already existing
      let card = document.getElementById(address);
      if (!card) {
        //take care of text colours
        if (colorsAmount < 5) {
          colors[address] = defaultcolors[colorsAmount];
        } else {
          colors[address] = "rgb(" + Math.floor(Math.random() * 155) + "," + Math.floor(Math.random() * 155) + "," + Math.floor(Math.random() * 155) + ")";
        }
        colorsAmount++;
        debug("assign color " + colors[address] + " to " + address);
        //add cards to html
        $('#mapping').append(
          `
          <div class="card mb-2" id=${address}>
            <div class="card-header">
              <h3 class="card-title d-inline" style="color:${colors[address]}">${dataGetServer.hostname}</h3>
              <h6 class="text-secondary d-inline">${address}</h6>
              <table class="table text-center">
                <thead>
                  <tr class="bg-white">
                    <td><h5 data-i18n-key="comingFrom">Coming from</h5></td>
                    <td><h5 data-i18n-key="domain">Domain</h5></td>
                    <td><h5 data-i18n-key="goingTo">Going to</h5></td>
                  </tr>
                </thead>
                <tbody name="in">     </tbody>
                  <tr class="bg-white" style="border-bottom:2px dotted grey">
                  </tr>
                <tbody name="out">    </tbody>
                  <tr class="bg-white" style="border-bottom:2px dotted grey">
                  </tr>
                <tbody name="all">
                <tr class="smallerTr text-secondary">
                  <td><i>(...)--></i></td>
                  <td><i data-i18n-key="anyOther">any other domain</i></td>
                  <td><i>-->(...)</i></td></td>
                </tr>
                <tr class="bg-white">
                </tr>
              </tbody>
              </table>
            </div>
          </div>
          `);
      }
    }
    debug(dataGetServers);
    debug(server);
    // Reset
    $('#Transport').html('');
    // Get transport API
    let getTransports = await GetTransport();
    if (getTransports[0]) {
      let getTransport = getTransports[1];
      debug(getTransport);
      // First foreach adds rows
      for (let transport of getTransport) {
        // format transport hostname address & nexthop address
        let addressIn = transport.hostname + "." + transport.domain + (transport.domain.endsWith(".") ? "" : ".");
        let addressOut = transport.nexthop.split(":")[0] + (transport.nexthop.split(":")[0].endsWith(".") ? "" : ".");
        debug("adding line FROM= " + addressIn + " WITH= " + transport.search + " TO= " + addressOut);
        // add it to mapping by route
        if (typeof domains[transport.search] === "undefined") {
          domains[transport.search] = [];
        }
        domains[transport.search].push([addressIn, addressOut]);

        // add it to mapping by server
        let cardIn = document.getElementById(addressIn).querySelector("tbody[name=in]");
        debug("cardIn : ");
        debug(cardIn);
        cardIn.innerHTML += `
          <tr>
          <td>(...)--></td>
          <td class="myAnchor">${transport.search}</td>
          <td style="color:${colors[addressOut]}" class="myAnchor">${addressOut}</td>
          </tr>
          `
        let cardOut = document.getElementById(addressOut);
        if (cardOut) {
          debug("cardOut : ");
          debug(cardOut.querySelector("tbody[name=out]"));
          cardOut.querySelector("tbody[name=out]").innerHTML += `
          <tr>
          <td style="color:${colors[addressIn]}" class="myAnchor">${addressIn}</td>
          <td class="myAnchor">${transport.search}</td>
          <td>-->(...)</td>
          </tr>
          `
        }

        // translate data into readable content
        var trClass = '';
        // Règle non appliqué car le cli ne s'est pas connecté depuis l'ajout
        if (server[transport['hostname']] < moment(transport['created_at']).unix()) {
          trClass = 'text-secondary';
          transport['hostname_td'] = '<abbr data-title="Not applied yet (last cli update : ' + moment.unix(server[transport['hostname']]).format("YYYY-MM-DD HH:MM:SS") + ')">' + transport['hostname'] + '</abbr>';
        } else {
          transport['hostname_td'] = transport['hostname'];
        }
        // add it to list
        $('#Transport').append(
          `
          <tr class="${trClass} text-black"" name="row" data-id="${transport.id}" data-hostname="${transport.hostname}">
            <td class="text-center" style="color:${colors[addressIn]}">${transport.hostname}</td>
            <td class="text-center">${transport.search_regex == 1 ? "regex" : "normal"}</td>
            <td class="text-center">${transport.search}</td>
            <td class="text-center" style="color:${colors[addressOut]}">${transport.transport}:${transport.nexthop}</td>
            <td class="text-center" name="email">${transport.email}</td>
            <td class="text-center">${moment(transport.created_at).format("YYYY-MM-DD")}</td>
            <td class="btn-group">
              <button type='button' class='btn btn-sm btn-danger delete delete-button'>
                <i class='fas fa-trash me-1'></i>
              </button>
            </td>
          </tr>
          `);
      };

      //map by search domain
      /*
      $('#routemapping').html('');
      for (const domain in domains) {
        $('#routemapping').append(
          `
          <div class="card mb-2" id=${domain}>
            <div class="card-header">
              <h3 class="card-title d-inline">${domain}</h3>
              <h6 class="mx-3 d-inline text-secondary" data-i18n-key="routeCount">independent routes : </h6>
            </div>
            <div class="card-body d-inline" name="ways"></div>
          </div>
          `);
        const routes = domains[domain];
        let tree = buildTree(routes);
        debug('tree ' + domain + ' = ');
        debug(tree);
        let count = 0;
        let text = '';
        for (const endPoint of rootAddress) {
          //to make sure we only draw each branche once for each endPoint, we call recursiv function for sub-branches. Top branch (last step) is filled down here
          text += (count==0 ?'' : '<br>');
          text += displayTree(tree[endPoint], endPoint, colors);
          text += `<span>--></span>`;
          text += `<span style="color:${colors[endPoint]}" class="myAnchor">${endPoint}</span>`;
          text += `<span>-->(...)</span><br>`;
          count++;
        }
        const cardDomain = document.getElementById(domain);
        cardDomain.querySelector("h6").textContent += count;
        cardDomain.querySelector("div[name=ways]").innerHTML = text;
      }
      */
      // add functions to built html
      $('.myAnchor').on("click", function () {
        debug("focusing on id = " + this.innerHTML);
        var target = document.getElementById(this.innerHTML);
        if (target){
          target.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
        }
      });

      $('.myAnchor').hover(function () {
        $(this).css('cursor', 'pointer');
      });
      $(".delete").on("click", function () {
        debug("id=" + $(this).parent().parent().data('id'));
        debug("hostname=" + $(this).parent().parent().data('hostname'));
        Delete(
          $(this).parent().parent().data('id'),
          $(this).parent().parent().data('hostname')
        );
      });
      alert.hide();
    } else {
      alert.removeClass().addClass("alert alert-danger");
      alert.html(getTransports[2].status + " : " + getTransports[1]);
    }
  } else {
    alert.removeClass().addClass("alert alert-danger");
    alert.html(getServer[2].status + " : " + getServer[1]);
  }
}

function buildTree(routes) {
  // Create a dictionary to store the children of each parent
  const childrenByParent = {};

  // Iterate over each route in the list
  routes.forEach(route => {
    const [parent, child] = route;

    // Add the child to the list of children for the parent
    if (childrenByParent[parent]) {
      childrenByParent[parent].push(child);
    } else {
      childrenByParent[parent] = [child];
    }
  });

  // Recursive function to build the tree
  function buildSubtree(parent) {
    const children = childrenByParent[parent] || [];

    // For each child, create a node and add its children recursively
    const subtree = children.map(child => {
      return { value: child, children: buildSubtree(child) };
    });

    return subtree;
  }

  // Find the root of the tree (the parent that has no parent itself)
  const root = routes.find(route => !routes.some(([parent2, child2]) => child2 === route[0]))[0];

  // Build the tree from the root
  const tree = buildSubtree(root);

  return tree;
}


function displayTree(tree, node, colors) {
  let result = '';
  const keys = Object.keys(tree);
  for (let i = 0; i < keys.length; i++) {
    const adress = keys[i];
    const branch = tree[adress];
    if (branch !== null) {
      //add new node before branch
      result = 
        `
        <span>--></span>
        <span style="color:${colors[adress]}" class="myAnchor">${adress}</span>
        ` + result;
      //call sub branches
      result =
        displayTree(branch, adress, colors)
        + result;
    } else {
      //call end of branch before node before branch
      result = 
        `<span>(...)--></span>
        <span style="color:${colors[adress]}" class="myAnchor">${adress}</span>
        ${(keys.length > 1 && i == keys.length - 1) ? '<br>' : ''}
        ` + result;

    }
  }
  return result;
}


// Handle deletion
async function Delete(id, hostname) {
  // Show loading spinner
  alert.show();
  alert.removeClass().addClass("alert alert-info");
  alert.html("Deleting data...");
  // Handle the API response
  let deleteTry = await DeleteTransport(id, hostname);
  if (deleteTry[0]) {
    alert.removeClass().addClass("alert alert-success");
    alert.html("Remove success !");
    UpdateTransportList();
  } else {
    alert.removeClass().addClass("alert alert-danger");
    alert.html(deleteTry[2].status + " : " + deleteTry[1]);
  }
};

// Handle adding action
async function Add() {
  // Show loading spinner
  alert.show();
  alert.removeClass().addClass("alert alert-info");
  alert.html("Adding data...");
  // Handle the API response
  let addinTry = await AddTransport(
    $('#hostname').val(),
    $('#search_regex').val(),
    $('#search').val(),
    $('#nexthop').val()
  );
  if (addinTry[0]) {
    alert.removeClass().addClass("alert alert-success");
    alert.html("Adding success !");
    UpdateTransportList();
  } else {
    debug(addinTry);
    alert.removeClass().addClass("alert alert-danger");
    alert.html(addinTry[2].status + " : " + addinTry[1]);
  }
};

// Listener sur le formulaire d'ajout
$("#add").on("click", function () {
  Add();
});
// Listener su rles input
$('input').keypress(function (e) {
  if (e.which == 13) {
    Add();
  }
});
// Hide alert if click
$("#alert").on("click", function () {
  $("#alert").hide();
});

$("#switchMapway").on("click", function () {
  $("#mapway").toggle();
});

$("#switchMapserv").on("click", function () {
  $("#mapserv").toggle();
});

