import { IsLoggedIn, Register } from "./api_connector.js";
import settings from './settings.js';

// Check if already loggedIn
if (IsLoggedIn()) {
  window.location.replace("index.html");
}

// Use the submit
document.querySelector('#registerForm').addEventListener('submit', async (event) => {
  event.preventDefault();

  // Show loading spinner
  const loadingSpinner = document.getElementById("loading-spinner");
  loadingSpinner.style.display = "block";
  
  let email = document.querySelector('#inputEmail').value;
  let password = document.querySelector('#inputPassword').value;
  let passwordConfirm = document.querySelector('#inputPasswordConfirm').value;
  const errorMessage = document.getElementById("errorMessage");

  // Clear previous & show messages
  errorMessage.textContent = "";
  if (password.length < settings.password_min_length) {
    errorMessage.textContent += "Please enter a password of at least " + settings.password_min_length + " characters. ";
  }
  if (email.length === "") {
    errorMessage.textContent += "Please enter an email. ";
  } else if (!settings.emailRegex.test(email)) {
    errorMessage.textContent += "Please enter a valid email adress. ";
  }
  if (password != passwordConfirm) {
    errorMessage.textContent += "Please repeat your password. ";
  }
  if (errorMessage.textContent != "") {
    return;
  };
  
  // Handle the API response
  let registerTry = await Register(email, password);
  if (registerTry[0]) {
    email = "";
    password = "";
    passwordConfirm = "";
    alert(" OK ! New user successfully created ");
    window.location.replace("login.html");
  } else {
    const errorReading = settings.errorcodes[registerTry[1]];
    errorMessage.innerHTML = errorReading ? (errorReading + " ( Message from server : " + registerTry[2] + " )") : "Server connection failed.";
  }


  // Hide loading spinner
  loadingSpinner.style.display = "none";
});